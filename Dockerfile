############################
# STEP 1 build executable binary
############################
FROM golang:alpine AS builder
# Install git (required for fetching the dependencies.)
RUN apk update && apk add --no-cache git
# Create appuser.
RUN adduser -D -g '' appuser
# Create buil dir
RUN mkdir /build
WORKDIR /build
# COPY go.mod and go.sum files to the workspace
COPY go.mod . 
COPY go.sum .

RUN go mod download
# COPY the source code as the last step
COPY . .
# Build the binary.
RUN GOOS=linux CGO_ENABLED=0 GOARCH=amd64 go build -ldflags="-w -s" -o /build/bin/server
############################
# STEP 2 build a small image
############################
FROM alpine
WORKDIR /go
# Import from builder.
# COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# Import the user and group files from the builder.
COPY --from=builder /etc/passwd /etc/passwd
# Copy our static executable.
COPY --from=builder /build/bin/server /go/bin/server
# Use an unprivileged user.
USER appuser
# Run the server binary.
CMD ["/go/bin/server"]