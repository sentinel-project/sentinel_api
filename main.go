package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"sentinel/server/controllers"
	"sentinel/server/services/authentication"
	"sentinel/server/services/logger"
	"sentinel/server/services/repository"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var userdao = repository.UsersDAO{}
var exchangedao = repository.ExchangeDAO{}
var balancedao = repository.BalanceDAO{}
var pricesdao = repository.PricesDAO{}
var quotesdao = repository.QuotesDAO{}
var tradesdao = repository.TradesDAO{}
var comchanneldao = repository.CommChannelDAO{}

// DbURL represent the base cluster url
var DbURL = os.Getenv("DB_URL")

// DbNAME represent the database name
var DbNAME = os.Getenv("DB_NAME")

// AppPORT represent the listening port
var AppPORT = os.Getenv("PORT")

func init() {
	if DbURL == "" {
		DbURL = "mongodb://localhost:27017"
	}
	if DbNAME == "" {
		DbNAME = "users_dev"
	}
	if AppPORT == "" {
		AppPORT = "3000"
	}
	// DB init
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(DbURL))
	if err != nil {
		logger.LogError("Init ()", err.Error())
	}
	userdao.Server = DbURL
	userdao.Client = client
	userdao.Database = DbNAME
	exchangedao.Server = DbURL
	exchangedao.Client = client
	exchangedao.Database = DbNAME
	balancedao.Server = DbURL
	balancedao.Client = client
	balancedao.Database = DbNAME
	pricesdao.Server = DbURL
	pricesdao.Client = client
	pricesdao.Database = DbNAME
	quotesdao.Client = client
	quotesdao.Server = DbURL
	quotesdao.Database = DbNAME
	tradesdao.Client = client
	tradesdao.Server = DbURL
	tradesdao.Database = DbNAME
	// comchanneldao.Client = client
	// comchanneldao.Server = DbURL
	// comchanneldao.Database = DbNAME
	userdao.Connect()
	exchangedao.ExchangeConnect()
	balancedao.BalanceConnect()
	pricesdao.PricesConnect()
	quotesdao.QuotesConnect()
	tradesdao.TradesConnect()
	// comchanneldao.CommChannelsConnect()
}

// Define HTTP request routes
func main() {
	r := mux.NewRouter()

	// Catch all route
	r.HandleFunc("/", controllers.Version)

	// Authentication method
	r.HandleFunc("/auth", controllers.Login).Methods("POST")

	// User methods
	r.HandleFunc("/user", controllers.CreateUser).Methods("POST")
	r.HandleFunc("/user", authentication.Verify(controllers.UpdateUser)).Methods("PUT")
	r.HandleFunc("/user", authentication.Verify(controllers.GetUser)).Methods("GET")

	// Exchanges methods
	r.HandleFunc("/exchange", authentication.Verify(controllers.GetExchange)).Methods("GET")
	r.HandleFunc("/exchange", authentication.Verify(controllers.CreateExchange)).Methods("POST")
	r.HandleFunc("/exchange/prices", authentication.Verify(controllers.GetBinancePrices)).Methods("GET")
	r.HandleFunc("/exchange/markets", authentication.Verify(controllers.GetBinanceMarkets)).Methods("GET")
	r.HandleFunc("/exchange/balances", authentication.Verify(controllers.GetBinanceWallet)).Methods("GET")
	r.HandleFunc("/exchange/trades", authentication.Verify(controllers.GetBinanceTrades)).Methods("GET")
	r.HandleFunc("/exchange/histo/{base}/{quote}", authentication.Verify(controllers.GetHistoPrices)).Methods("GET")
	r.HandleFunc("/exchange/get/{base}/toeuro", authentication.Verify(controllers.GetBitcoinToEuro)).Methods("GET")

	// Balances methods
	r.HandleFunc("/balance", authentication.Verify(controllers.GetBalances)).Methods("GET")
	r.HandleFunc("/balance/init", authentication.Verify(controllers.InitBalances)).Methods("GET")
	r.HandleFunc("/balance/update", authentication.Verify(controllers.UpdateBalances)).Methods("GET")
	r.HandleFunc("/balance/quotes", authentication.Verify(controllers.GetQuotes)).Methods("GET")
	r.HandleFunc("/balance/markets", authentication.Verify(controllers.GetTradableMarkets)).Methods("GET")
	r.HandleFunc("/balance/markets/update", authentication.Verify(controllers.UpdateMarkets)).Methods("GET")

	// Quotes methods
	r.HandleFunc("/quotes", authentication.Verify(controllers.GetQuotes)).Methods("GET")
	r.HandleFunc("/quotes/update", authentication.Verify(controllers.RefreshQuotes)).Methods("GET")
	r.HandleFunc("/quotes/by/sym/{symbol}", authentication.Verify(controllers.GetQuotesBySymbol)).Methods("GET")
	r.HandleFunc("/quotes/by/asset/{asset}", authentication.Verify(controllers.GetQuotesByAsset)).Methods("GET")

	// Prices methods
	r.HandleFunc("/prices", authentication.Verify(controllers.GetPrices)).Methods("GET")
	r.HandleFunc("/prices/update", authentication.Verify(controllers.RefreshPrices)).Methods("GET")
	r.HandleFunc("/prices/by/sym/{symbol}", authentication.Verify(controllers.GetPricesBySymbol)).Methods("GET")

	// Trades methods
	r.HandleFunc("/trades", authentication.Verify(controllers.GetTrades)).Methods("GET")
	r.HandleFunc("/trades/get/by/sym/{symbol}", authentication.Verify(controllers.GetTradesBySymbol)).Methods("GET")
	r.HandleFunc("/trades/update/by/sym/{symbol}", authentication.Verify(controllers.UpdateTradesBySymbol)).Methods("GET")

	// Stats methods
	r.HandleFunc("/stats", authentication.Verify(controllers.GetStats)).Methods("GET")
	r.HandleFunc("/stats/evo", authentication.Verify(controllers.GetEvo)).Methods("GET")
	r.HandleFunc("/stats/trades/by/sym/{symbol}", authentication.Verify(controllers.GetStatsForSymbol)).Methods("GET")

	// Comchannel methods (WIP)
	r.HandleFunc("/msgaccounts", authentication.Verify(controllers.GetMsgAccount)).Methods("GET")
	r.HandleFunc("/msgaccounts", authentication.Verify(controllers.CreateMsgAccount)).Methods("POST")
	r.HandleFunc("/msgaccounts/msg", authentication.Verify(controllers.SendTestMessage)).Methods("GET")

	// Catching interrupt (ctr-c) signal
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	// With this handler
	go func() {
		for sig := range c {
			msg := fmt.Sprintf("Catch sig [%v]\n\n\tSee you ! :)\n", sig)
			logger.LogInfo("main.go:81", msg)
			os.Exit(0)
		}
	}()
	// Server started
	logs := fmt.Sprintf("Server listening at http://%s:%s", "0.0.0.0", AppPORT)
	logger.LogInfo("main.go:118", logs)
	if err := http.ListenAndServe(":"+AppPORT, r); err != nil {
		log.Fatal(err)
	}
}
