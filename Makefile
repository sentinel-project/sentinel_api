####
## Sentinel Go API
##     Makefile
## (run $ make for help)
####
.PHONY: help start

ADB		= adb
RM		= rm -f
FLUTTER = flutter
CLEAN   = flutter clean
SHELL   = /usr/bin/bash
ENV     = $(source .prod.env)
APP_NAME := $(shell head -n 1 README.md | cut -d ' ' -f2 |  tr '[:upper:]' '[:lower:]')
APP_VSN := $(shell git describe --tags | cut -d '-' -f1)
BUILD := $(shell git rev-parse --short HEAD)
HEROKU_APP = blooming-ocean-31863

ADB_ARGS	= reverse tcp:3000 tcp:3000
GBUILD_ENV := CGO_ENABLED=0 GOOS=linux GOARCH=amd64

help:
	@echo -e "\t$(APP_NAME):$(APP_VSN)-$(BUILD) \033[1mmake\033[0m options:\n"
	@perl -nle 'print $& if m{^[a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "- \033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo -e "\n"

default: help

build: ## Build the go package
	$(GBUILD_ENV) go build -ldflags="-w -s" -o bin/server

image: ## Build the Docker image
	docker build   \
    	-t $(APP_NAME):$(APP_VSN)-$(BUILD) \
    	-t $(APP_NAME):latest .

serve: ## Run the app in dev mode
	@docker ps &>/dev/null || sudo systemctl start docker
	# @docker start mongo &>/dev/null || \
	# 	   docker run -d \
	# 	   	-p 27017:27017 \
	# 		--name mongo \
	# 		mvertes/alpine-mongo
	@$$(RSA_PUBKEY=`cat app.rsa.pub` RSA_PRIVKEY=`cat app.rsa` DB_URL=$$DB_URL go run . &>server.log & echo $$! > server.pid)
	@echo -e "\n\t🏄  $(APP_NAME) API Started with PID [$$(cat server.pid)]"
	@echo -e "\t👁   Watch logs in server.log\n"

stop: ## Stop the running dev server
	@kill $$(cat server.pid) &>/dev/null && \
		echo -e "Server Successfully stopped" || \
		echo -e "\n\tServer allready stopped :)\n"

deploy: ## Deploy container image to heroku
	heroku container:push web --app $(HEROKU_APP)
	heroku container:release web --app $(HEROKU_APP)

clean: ## Remove server logs file
	@rm server.log server.pid 2&>/dev/null && echo -e "\tClean logs 💨"  || echo -e "\t..."
	@rm -rf bin 2&>/dev/null && echo -e "\tClean build 💨" || echo "build allready clean"


dbclean: ## Kill and restart mongo db container
	@docker stop mongo
	@docker rm mongo
	@docker run -d \
		   	-port 27017:27017 \
			--name mongo \
			mvertes/alpine-mongo
	@echo -e "\n\tSuccessfully reset mongo db"
	# @echo -e "\tRemember to run 'make db' before using db\n"