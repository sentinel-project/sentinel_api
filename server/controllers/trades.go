package controllers

import (
	"fmt"
	"net/http"

	"sentinel/server/services/exchanges"
	"sentinel/server/services/repository"
	"sentinel/server/services/response"

	"github.com/gorilla/context"
	"github.com/gorilla/mux"
)

var tradesdao = repository.TradesDAO{}

// GetTrades return saved user trades history
func GetTrades(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	account, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "No api account. Please setup at least one before!")
		return
	}
	trades, err := tradesdao.GetTradesByExchange(account.ID.Hex())
	if err != nil {
		response.RespondWithError(w, 401, "unhautorized")
		return
	}
	if trades == nil {
		response.RespondWithError(w, 401, "No trades found, try /trades/get/by/sym/[symbol]")
		return
	}
	response.RespondWithJSON(w, 200, trades)
}

// GetTradesBySymbol return saved user trades history
func GetTradesBySymbol(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	symb := mux.Vars(r)
	symbol := symb["symbol"]
	account, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "No api account. Please setup at least one before!")
		return
	}
	trades, err := tradesdao.GetTradesBySymbol(account.ID.Hex(), symbol)
	if err != nil {
		response.RespondWithError(w, 401, "No such symbol")
		return
	}
	if trades == nil {
		response.RespondWithError(w, 401, "No trades found, try /trades/update/by/sym/[symbol]")
		return
	}
	response.RespondWithJSON(w, 200, trades)
}

// UpdateTradesBySymbol load trade history and save them in db
func UpdateTradesBySymbol(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	symb := mux.Vars(r)
	symbol := symb["symbol"]
	account, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "No api account. Please setup at least one before!")
		return
	}
	trades, err := exchanges.GetLastBinanceTradesBySymbol(account, symbol)
	if err != nil {
		response.RespondWithError(w, 401, "Error retrieving trades, check your exchanges setup!")
		return
	}
	err = tradesdao.UpdateTrades(account.ID.Hex(), trades)
	if err != nil {
		response.RespondWithError(w, 401, "Error retrieving trades, check your exchanges setup!")
		return
	}
	msg := fmt.Sprintf("%v trades updated", len(trades))
	response.RespondWithJSON(w, 200, map[string]string{"success": msg})
}
