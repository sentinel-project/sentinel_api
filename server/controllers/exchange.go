package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"sentinel/server/models"
	"sentinel/server/services/exchanges"
	"sentinel/server/services/logger"
	"sentinel/server/services/repository"
	"sentinel/server/services/response"

	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var exchangedao = repository.ExchangeDAO{}

// GetExchange get currently authenticated user exchanges
func GetExchange(w http.ResponseWriter, r *http.Request) {
	// Get all by param ?
	var err error
	var exchanges []models.Exchange
	uid := context.Get(r, "uid")
	exchanges, err = exchangedao.FindUserAccounts(fmt.Sprintf("%s", uid))
	if err != nil {
		response.RespondWithError(w, 401, "unhautorized")
		return
	}
	if exchanges == nil {
		response.RespondWithError(w, 401, "No such exchange, please add one")
		return
	}
	// Format for sensitives datas
	for i := range exchanges {
		lpubk := len(exchanges[i].PublicKey) / 2
		lprivk := len(exchanges[i].PrivateKey) / 2
		b := make([]rune, int(lprivk))
		for j := range b {
			b[j] = '*'
		}
		exchanges[i].PublicKey = exchanges[i].PublicKey[0:int(lpubk)] + "..."
		exchanges[i].PrivateKey = string(b)
	}
	response.RespondWithJSON(w, 200, exchanges)
}

// CreateExchange create a new echange in db
func CreateExchange(w http.ResponseWriter, r *http.Request) {
	// TODO: To be refactored !!
	var data models.Exchange
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	usr := context.Get(r, "uid")
	data.UserID = fmt.Sprintf("%s", usr)
	logger.LogInfo("CreateExchange ()", fmt.Sprintf("%v", data))
	if err == nil {
		nexchange := models.Exchange{
			ID:         primitive.NewObjectID(),
			Name:       data.Name,
			UserID:     data.UserID,
			PublicKey:  data.PublicKey,
			PrivateKey: data.PrivateKey,
		}
		err = exchangedao.InsertExchange(nexchange)
		if err == nil {
			response.RespondWithJSON(w, 200, nexchange)
			return
		}
		logger.LogError("CreateExchange()", err.Error())
		response.RespondWithError(w, 401, "unauthorized")
		return
	}
	logger.LogError("CreateExchange()", err.Error())
	ercode := strings.Split(err.Error(), " ")[0]
	if ercode == "E11000" {
		response.RespondWithError(w, 401, "Already added")
		return
	}
	response.RespondWithError(w, 401, "unauthorized")
}

// GetBinanceMarkets return tradable markets on binance exchange
func GetBinanceMarkets(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	account, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	res, err := exchanges.GetBinanceSymbols(account)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	response.RespondWithJSON(w, 200, res)
}

// GetBinanceWallet return user live balances on binance exchange
func GetBinanceWallet(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	account, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	res, err := exchanges.GetBinanceBalances(account)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	response.RespondWithJSON(w, 200, res)
}

// GetBinancePrices return live quotes on binance exchange
func GetBinancePrices(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	account, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	res, err := exchanges.GetBinancePrices(account)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	response.RespondWithJSON(w, 200, res)
}

// GetBinanceTrades return user last trades on binance exchange
func GetBinanceTrades(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	account, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	res, err := exchanges.GetLastBinanceTradesBySymbol(account, "NEOBTC")
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	response.RespondWithJSON(w, 200, res)
}

// GetHistoPrices return historical prices on one base against one quote
func GetHistoPrices(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	param := mux.Vars(r)
	base := param["base"]
	quote := param["quote"]
	account, err := exchangedao.GetUserAccount("cryptocompare", uid)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	res, err := exchanges.GetDailyPrices(account, base, quote)
	if err != nil {
		response.RespondWithError(w, 401, "Api error")
		return
	}
	response.RespondWithJSON(w, 200, res)
}

// GetBitcoinToEuro return live conversion for a price in btc
func GetBitcoinToEuro(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	param := mux.Vars(r)
	btc := param["base"]
	btcval, err := strconv.ParseFloat(btc, 64)
	account, err := exchangedao.GetUserAccount("cryptocompare", uid)
	fmt.Printf("Cc acount : %v\n", account.PrivateKey)
	if err != nil || account.PrivateKey == "" {
		response.RespondWithError(w, 401, "Unhautorized")
		return
	}
	resp, err := exchanges.FromBitcoinToEuro(account, btcval)
	if err != nil {
		logger.LogError("GetBitcoinToEuro : ", err.Error())
		response.RespondWithError(w, 401, "No such account")
		return
	}
	response.RespondWithJSON(w, 200, map[string]float64{"success": resp})
}
