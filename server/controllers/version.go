package controllers

import (
	"net/http"

	"sentinel/server/services/response"
)

type message struct {
	// ID      primitive.ObjectID `json:"id"`
	Name    string `json:"name"`
	Version string `json:"version"`
	Doc     string `json:"doc"`
	Updated int64  `json:"updated"`
}

// Version is returning api current version (used as a 'catch-all' route)
func Version(w http.ResponseWriter, r *http.Request) {
	m := message{"sentinel API", "v0.1.1", "https://api.doc.sentinel.xyz/", 0}
	response.RespondWithJSON(w, http.StatusOK, m)
}
