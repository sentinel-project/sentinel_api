package controllers

import (
	"fmt"
	"net/http"

	"sentinel/server/models"
	"sentinel/server/services/logger"
	"sentinel/server/services/response"

	"github.com/gorilla/context"
	"github.com/gorilla/mux"
)

// GetQuotes return a user balances quotes on each market
func GetQuotes(w http.ResponseWriter, r *http.Request) {
	var filt []models.Quotes
	var err error
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	filt, err = quotesdao.GetQuotesByUserID(uid)
	if err != nil {
		response.RespondWithError(w, 401, "Error")
		return
	}
	if filt == nil {
		response.RespondWithError(w, 401, "No quotes found. Try quotes/init first")
		return
	}
	response.RespondWithJSON(w, 200, filt)
}

// GetQuotesByAsset return all saved prices for a user balance asset
func GetQuotesByAsset(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	vars := mux.Vars(r)
	asset := vars["asset"]
	account, err := balancedao.GetUserBalances(uid)
	if err != nil {
		response.RespondWithError(w, 401, "No such balances")
		return
	}
	var acc models.Balance
	for _, a := range account {
		if a.Asset == asset {
			acc = a
		}
	}
	// Todo: check if acc exist
	prices, err := quotesdao.GetQuotesByBalanceID(acc.ID.Hex())
	if err != nil {
		response.RespondWithError(w, 401, "No such quotes")
		return
	}
	response.RespondWithJSON(w, 200, prices)
}

// GetQuotesBySymbol return all saved prices for a trading pair symbol
func GetQuotesBySymbol(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	vars := mux.Vars(r)
	symbol := vars["symbol"]
	account, err := balancedao.GetUserBalances(uid)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	var acc models.Balance
	for _, a := range account {
		for _, b := range a.Markets {
			if b == symbol {
				acc = a
				break
			}
		}
	}
	// Todo: check if acc exist
	prices, err := quotesdao.GetQuotesByPair(symbol, acc.ID.Hex())
	if err != nil {
		response.RespondWithError(w, 401, "No such prices")
		return
	}
	response.RespondWithJSON(w, 200, prices)
}

// RefreshQuotes refresh user quotes on each market
func RefreshQuotes(w http.ResponseWriter, r *http.Request) {
	var filt []models.Quotes
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	bals, err := balancedao.GetUserBalances(uid)
	prices, err := pricedao.GetPricesByExchange(bals[0].ExchangeID)
	if err != nil {
		logger.LogError("RefreshQuotes ()", err.Error())
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	if prices == nil {
		response.RespondWithError(w, 401, "No prices recorded. Try /prices/init first")
		return
	}
	filt, err = quotesdao.CalcQuotes(bals, prices)
	oldq, err := quotesdao.GetQuotesByExchangeID(bals[0].ExchangeID)
	if len(filt) == len(oldq) {
		response.RespondWithJSON(w, 200, map[string]string{"success": "Allready up to date!"})
		return
	}
	if len(oldq) == 0 {
		err = quotesdao.InsertQuotes(false, filt)
	} else {
		err = quotesdao.InsertQuotes(true, filt)
	}
	if err != nil {
		logger.LogError("RefreshQuotes ()", err.Error())
		response.RespondWithError(w, 401, "Invalid prices or balances, please init before")
		return
	}
	msg := fmt.Sprintf("%v quotes successfully recorded", len(filt))
	response.RespondWithJSON(w, 200, map[string]string{"success": msg})
}
