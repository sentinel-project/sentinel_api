package controllers

import (
	"fmt"
	"net/http"

	"sentinel/server/models"
	"sentinel/server/services/response"
	"sentinel/server/services/statistics"

	"github.com/gorilla/context"
	"github.com/gorilla/mux"
)

// GetStats return a user btc sum
func GetStats(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	// TODO: Must be rewrite to include all user api accounts
	// and also to be less complex ;)
	account, err := exchangesdao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "Please setup exchange api account first")
		return
	}
	existbal, err := balancedao.GetBalancesByEID(account.ID.Hex())
	if err != nil {
		response.RespondWithError(w, 401, "Please get balances first")
		return
	}
	existquotes, err := quotesdao.GetQuotesByExchangeID(account.ID.Hex())
	if err != nil {
		response.RespondWithError(w, 401, "Please get quotes first")
		return
	}
	ress, err := statistics.GetStats(existbal, existquotes)
	if err != nil {
		response.RespondWithError(w, 401, "Unable to get statistics")
		return
	}
	response.RespondWithJSON(w, 200, ress)
}

// GetEvo return a user btc sum
func GetEvo(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	// TODO: Must be rewrite to include all user api accounts
	// and also to be less complex ;)
	account, err := exchangesdao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "Please setup exchange api account first")
		return
	}
	existbal, err := balancedao.GetBalancesByEID(account.ID.Hex())
	if err != nil {
		response.RespondWithError(w, 401, "Please load your balances first")
		return
	}
	existquotes, err := quotesdao.GetQuotesByExchangeID(account.ID.Hex())
	if err != nil {
		response.RespondWithError(w, 401, "Please load quotes first")
		return
	}
	ress, err := statistics.GetEvoStats(existbal, existquotes)
	if err != nil {
		response.RespondWithError(w, 401, "Failed to get evo stats")
		return
	}
	response.RespondWithJSON(w, 200, ress)
}

// GetStatsForSymbol return a user analysis of his trades history for sym symbol
func GetStatsForSymbol(w http.ResponseWriter, r *http.Request) {
	var err error
	var account models.Exchange
	var trades []models.Trade
	var prices []models.Prices
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	symb := mux.Vars(r)
	symbol := symb["symbol"]
	account, err = exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "No accounts , please add one")
	}
	trades, err = tradesdao.GetTradesBySymbol(account.ID.Hex(), symbol)
	if err != nil || len(trades) == 0 {
		response.RespondWithError(w, 401, "No trades for symbol found")
		return
	}
	prices, err = pricedao.GetPricesbySymbol(account.ID.Hex(), symbol)
	if err != nil || len(prices) == 0 {
		response.RespondWithError(w, 401, "No quotes for symbol found")
		return
	}
	resp, err := statistics.GetTradingStatsBySymbol(trades, prices)
	if err != nil {
		response.RespondWithError(w, 401, "Failed to get trading stats")
		return
	}
	response.RespondWithJSON(w, 200, resp)
}
