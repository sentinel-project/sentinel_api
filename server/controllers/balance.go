package controllers

import (
	"errors"
	"fmt"
	"net/http"

	"sentinel/server/models"
	"sentinel/server/services/exchanges"
	"sentinel/server/services/logger"
	"sentinel/server/services/repository"
	"sentinel/server/services/response"

	"github.com/gorilla/context"
)

var balancedao = repository.BalanceDAO{}
var quotesdao = repository.QuotesDAO{}
var exchangesdao = repository.ExchangeDAO{}

// InitBalances initialise a user assets balances
func InitBalances(w http.ResponseWriter, r *http.Request) {
	var nbals []models.Balance
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	// TODO: Must be rewrite to include all user api accounts
	// and also to be less complex ;)
	account, err := exchangesdao.GetUserAccount("binance", uid)
	existbal, err := balancedao.GetBalancesByEID(account.ID.Hex())
	if err == nil && existbal == nil {
		// Getting current balances datas from exchange (exchangeapi format)
		bibals, err := exchanges.GetBinanceBalances(account)
		if err != nil {
			logger.LogError("sentinel/server/controllers/balance_controller.go:29", err.Error())
			response.RespondWithError(w, 401, "unauthorized")
			return
		}
		// Conversion to exchange api format to our models.Balance fmt
		nbals, err = models.FromBinanceToBalance(bibals, account.ID.Hex(), account.UserID)
		if err != nil {
			logger.LogError("sentinel/server/controllers/balance_controller.go:35", err.Error())
			response.RespondWithError(w, 401, "unauthorized")
			return
		}
		// Saving in database on balance collection
		err = balancedao.InsertBalances(nbals)
		if err != nil {
			logger.LogError("sentinel/server/controllers/balance_controller.go:41", err.Error())
			response.RespondWithError(w, 401, "unauthorized")
			return
		}
		msg := fmt.Sprintf("updated : %v", len(nbals))
		response.RespondWithJSON(w, 200, map[string]string{"success": msg})
		return
	}
	if err == nil && existbal != nil {
		err = errors.New("Allready setup, do /update instead")
		logger.LogError("sentinel/server/controllers/balance_controller.go:23", err.Error())
		response.RespondWithError(w, 401, err.Error())
		return
	}
	logger.LogError("sentinel/server/controllers/balance_controller.go:23", err.Error())
	response.RespondWithError(w, 401, "unauthorized")
	return
}

// GetBalances return currently auth user balances
func GetBalances(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	bals, err := balancedao.GetUserBalances(uid)
	if err != nil || bals == nil {
		if bals == nil {
			err = errors.New("No balance recorded for user")
		}
		logger.LogError("GetBalances ()", err.Error())
		response.RespondWithError(w, 401, "No such balances")
		return
	}
	response.RespondWithJSON(w, 200, bals)
}

func getMarketsSlice(bals []models.Balance, sym []models.Symbols) []string {
	var mrkts []string
	for index := range bals {
		var foo []string
		for _, v := range sym {
			// if the trading symbol match the one in user asset
			if v.Base == bals[index].Asset {
				foo = append(foo, v.Symbol)
				// append trading pair to asset.Markets strings list
				mrkts = append(mrkts, v.Symbol)
			}
		}
	}
	return mrkts
}

func setMarketsSlice(bals []models.Balance, sym []models.Symbols) []models.Balance {
	mrkts := getMarketsSlice(bals, sym)
	for index := range bals {
		bals[index] = balancedao.SetMarkets(bals[index], mrkts)
	}
	return bals
}

// UpdateBalances update balances value
func UpdateBalances(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	// TODO: better check on error (**maybe** refacto the whole method) !!
	exch, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		logger.LogError("UpdateBalances ()", err.Error())
		response.RespondWithError(w, 401, "unauthorized")
		return
	}
	bals, err := balancedao.GetUserBalances(uid)
	if err != nil {
		logger.LogError("UpdateBalances ()", err.Error())
		response.RespondWithError(w, 401, "Missing balances try /balance/init first")
		return
	}
	data, err := exchanges.GetBinanceBalances(exch)
	if err != nil {
		logger.LogError("UpdateBalances ()", err.Error())
		response.RespondWithError(w, 401, "unauthorized")
		return
	}
	nbals, err := models.FromBinanceToBalance(data, exch.ID.Hex(), uid)
	for i, j := range nbals {
		for k, l := range bals {
			if j.Asset == l.Asset {
				nbals[i].ID = bals[k].ID
				nbals[i].Markets = bals[k].Markets
			}
		}
	}
	err = balancedao.UpdateBalances(nbals)
	if err != nil {
		logger.LogError("UpdateBalances ()", err.Error())
		response.RespondWithError(w, 401, "No such balances")
		return
	}
	response.RespondWithJSON(w, 200, map[string]string{"success": "Balances successfully updated"})
}

// UpdateMarkets update balances with available trading pairs
func UpdateMarkets(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	// TODO: better check on error (**maybe** refacto the whole method) !!
	exch, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		logger.LogError("UpdateMarkets ()", err.Error())
		response.RespondWithError(w, 401, "unauthorized")
		return
	}
	bals, err := balancedao.GetUserBalances(uid)
	if err != nil {
		logger.LogError("InitMarkets ()", err.Error())
		response.RespondWithError(w, 401, "Missing balances try /balance/init first")
		return
	}
	sym, err := exchanges.GetBinanceSymbols(exch)
	if err != nil {
		logger.LogError("InitMarkets ()", err.Error())
		response.RespondWithError(w, 401, "unauthorized")
		return
	}
	bals = setMarketsSlice(bals, sym)
	err = balancedao.UpdateBalances(bals)
	if err != nil {
		logger.LogError("InitMarkets ()", err.Error())
		response.RespondWithError(w, 401, "No such balances")
		return
	}
	response.RespondWithJSON(w, 200, map[string]string{"success": "Market successfully setup"})
}

// GetTradableMarkets return a list of available trading pair
func GetTradableMarkets(w http.ResponseWriter, r *http.Request) {
	var mrkts []string
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	exch, err := exchangedao.GetUserAccount("binance", uid)
	bals, err := balancedao.GetUserBalances(uid)
	sym, err := exchanges.GetBinanceSymbols(exch)
	if err != nil {
		logger.LogError("GetTradableMarkets ()", err.Error())
		response.RespondWithError(w, 401, "No such balances")
		return
	}
	mrkts = getMarketsSlice(bals, sym)
	response.RespondWithJSON(w, 200, mrkts)
}
