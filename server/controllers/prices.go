package controllers

import (
	"fmt"
	"net/http"

	"sentinel/server/services/exchanges"
	"sentinel/server/services/repository"
	"sentinel/server/services/response"

	"github.com/gorilla/context"
	"github.com/gorilla/mux"
)

var pricedao = repository.PricesDAO{}

// GetPrices return all saved prices for a user exchange
func GetPrices(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	account, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "No api account. Please setup at least one before!")
		return
	}
	prices, err := pricedao.GetPricesByExchange(account.ID.Hex())
	if err != nil || prices == nil {
		response.RespondWithError(w, 401, "No prices found. Try prices/update first")
		return
	}
	response.RespondWithJSON(w, 200, prices)
}

// GetPricesBySymbol return all saved prices for a user exchange
func GetPricesBySymbol(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	vars := mux.Vars(r)
	symb := vars["symbol"]
	account, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	prices, err := pricedao.GetPricesbySymbol(account.ID.Hex(), symb)
	if err != nil {
		response.RespondWithError(w, 401, "No such prices")
		return
	}
	response.RespondWithJSON(w, 200, prices)
}

// RefreshPrices save new prices info in db
func RefreshPrices(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	account, err := exchangedao.GetUserAccount("binance", uid)
	if err != nil {
		response.RespondWithError(w, 401, "No api account. Please setup at least one before!")
		return
	}
	data, err := exchanges.GetBinancePrices(account)
	if err != nil || data == nil {
		msg := fmt.Sprintf("Cannot fetch prices. Please check your [%s] account credentials", account.Name)
		response.RespondWithError(w, 401, msg)
		return
	}
	err = pricedao.InsertPrices(data)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	msg := fmt.Sprintf("%v prices updated", len(data))
	response.RespondWithJSON(w, 200, map[string]string{"success": msg})
}
