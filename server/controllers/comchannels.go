package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"sentinel/server/models"
	"sentinel/server/services/logger"
	"sentinel/server/services/messaging"
	"sentinel/server/services/repository"
	"sentinel/server/services/response"

	"github.com/gorilla/context"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var controllerdao = repository.CommChannelDAO{}

// GetMsgAccount return the user messages channels accounts
func GetMsgAccount(w http.ResponseWriter, r *http.Request) {
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	accounts, err := controllerdao.GetUserChannels(uid)
	if err != nil {
		logger.LogError("GetMsgAccount ()", err.Error())
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	response.RespondWithJSON(w, 200, accounts)
}

// CreateMsgAccount save a new messaging apis account for user
func CreateMsgAccount(w http.ResponseWriter, r *http.Request) {
	// To be refactored !! TODO
	var data models.CommChannel
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	if err == nil {
		naccount := models.CommChannel{
			ID:     primitive.NewObjectID(),
			UserID: uid,
			Name:   data.Name,
			Creds:  data.Creds,
		}
		err = controllerdao.InsertChannel(naccount)
		if err == nil {
			resp := fmt.Sprintf("[%s] api successfuly registered", naccount.Name)
			response.RespondWithJSON(w, 200, map[string]string{"success": resp})
			return
		}
		ercode := strings.Split(err.Error(), " ")[0]
		logger.LogError("CreateUser()", err.Error())
		errmsg := "unhautorized"
		fmt.Printf("code : %v\n", ercode)
		response.RespondWithError(w, 401, errmsg)
		return
	}
	logger.LogError("CreateUser()", err.Error())
	response.RespondWithError(w, 401, "unauthorized")
}

// SendTestMessage  fofo
func SendTestMessage(w http.ResponseWriter, r *http.Request) {
	var serv models.CommChannel
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	accounts, err := controllerdao.GetUserChannels(uid)
	if err != nil {
		response.RespondWithError(w, 401, "Unauthorized")
		return
	}
	for i, v := range accounts {
		if v.Name == "freemobile" {
			serv = accounts[i]
			break
		}
	}
	fab, err := messaging.FreeMobileSEND(serv)
	response.RespondWithJSON(w, 200, fab)
}
