package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"sentinel/server/models"
	"sentinel/server/services/authentication"
	"sentinel/server/services/logger"
	"sentinel/server/services/repository"
	"sentinel/server/services/response"

	"github.com/gorilla/context"
)

var userdao = repository.UsersDAO{}

/// TODO: Moove the next struct to a proper model folder

// AuthReq represent the login format
type AuthReq struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

const _authType = "Bearer"

// AuthRes represent the auth response format
type AuthRes struct {
	Token    string `json:"token"`
	AuthType string `json:"type"`
}

// GetUser set currently authenticated user id in context or return http error
func GetUser(w http.ResponseWriter, r *http.Request) {
	var err error
	var user models.User
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	user, err = userdao.FindByID(uid)
	if err != nil {
		response.RespondWithError(w, 401, "unhautorized")
		return
	}
	response.RespondWithJSON(w, 200, user)
}

/// TODO: add validators !!

// CreateUser save a new user to database
func CreateUser(w http.ResponseWriter, r *http.Request) {
	// To be refactored !! TODO
	var data models.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	if err == nil {
		nuser := models.NewUserFromObject(data)
		err = userdao.Insert(nuser)
		if err == nil {
			resp := fmt.Sprintf("%s successfuly registered, please check %s inbox", nuser.Name, nuser.Email)
			response.RespondWithJSON(w, 200, map[string]string{"success": resp})
			return
		}
		ercode := strings.Split(err.Error(), " ")[0]
		logger.LogError("CreateUser()", err.Error())
		errmsg := "unhautorized"
		if ercode == "E11000" {
			errmsg = "Invalid fields"
		}
		response.RespondWithError(w, 401, errmsg)
		return
	}
	logger.LogError("CreateUser()", err.Error())
	response.RespondWithError(w, 401, "unauthorized")
}

// UpdateUser update a user profile
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	var update models.User
	var err error
	inf := context.Get(r, "uid")
	uid := fmt.Sprintf("%s", inf)
	decode := json.NewDecoder(r.Body)
	err = decode.Decode(&update)
	if err != nil {
		logger.LogError("UpdateUser ()", err.Error())
		response.RespondWithError(w, 401, "unhautorized")
		return
	}
	err = userdao.Update(uid, update)

	response.RespondWithJSON(w, 200, map[string]string{"sucess": "Sucessfull update !"})
}

// Login return auth JWT token against username passord combo
func Login(w http.ResponseWriter, r *http.Request) {
	// TODO: To be refactored
	var data AuthReq
	var usr models.User
	var err error
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&data)
	usr, err = userdao.FindByName(data.Name)
	if err != nil {
		logger.LogError("Login()", err.Error())
		response.RespondWithError(w, 401, "unauthorized")
		return
	} else if !repository.ComparePasswords(usr.Password, []byte(data.Password)) {
		logger.LogError("Login()", "bad password")
		response.RespondWithError(w, 401, "bad password")
		return
	}
	token, err := authentication.CreateToken(usr, r.Header.Get("User-Agent"), logger.ReadUserIP(r))
	if err != nil {
		logger.LogError("Login()", err.Error())
		response.RespondWithError(w, 401, "error")
		return
	}
	response.RespondWithJSON(w, 200, &AuthRes{
		Token:    token,
		AuthType: _authType,
	})
}
