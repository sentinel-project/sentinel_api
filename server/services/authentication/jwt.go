package authentication

import (
	"crypto/rsa"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"sentinel/server/models"
	"sentinel/server/services/logger"
	"sentinel/server/services/response"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
)

// location of the files used for signing and verification
// (taken from [here](https://github.com/dgrijalva/jwt-go/blob/master/http_example_test.go))
const (
	privKeyPath = "./app.rsa"     // openssl genrsa -out app.rsa keysize
	pubKeyPath  = "./app.rsa.pub" // openssl rsa -in app.rsa -pubout > app.rsa.pub
)

// Keyfiles with format
var (
	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
)

const jwtservicedotgo = "server/services/authentication/jwt.go"

// JWTClaim represent the jwt token payload format
type JWTClaim struct {
	*jwt.StandardClaims
	*models.UserInfo
}

func init() {
	var err error
	signBytes := os.Getenv("RSA_PRIVKEY")
	// fatal(err)
	if err != nil || signBytes == "" {
		if signBytes == "" {
			err = errors.New("No such RSA_PRIVKEY")
		}
		logger.LogError("jwt init()", err.Error())
	}
	if signKey, err = jwt.ParseRSAPrivateKeyFromPEM([]byte(signBytes)); err != nil {
		logger.LogError(jwtservicedotgo+":51", err.Error())
	}
	// verifyBytes, err := ioutil.ReadFile(pubKeyPath)
	verifyBytes := os.Getenv("RSA_PUBKEY")
	// fatal(err)
	if err != nil || verifyBytes == "" {
		if verifyBytes == "" {
			err = errors.New("No such RSA_PUBKEY")
		}
		logger.LogError(jwtservicedotgo+":58", err.Error())
	}
	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM([]byte(verifyBytes))
	// fatal(err)
	if err != nil {
		logger.LogError(jwtservicedotgo+":63", err.Error())
	}
}

// CreateToken get the jwt to send after auth
func CreateToken(user models.User, ua string, addr string) (string, error) {
	// create a signer for rsa 256
	t := jwt.New(jwt.GetSigningMethod("RS256"))

	// set our claims
	t.Claims = &JWTClaim{
		&jwt.StandardClaims{
			Issuer: "Sentinel APi",
			// set the expire time
			// see http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20#section-4.1.4
			ExpiresAt: time.Now().Add(time.Minute * 180).Unix(),
			Subject:   fmt.Sprintf("name:%s;from:%s", user.Name, addr),
		},
		&models.UserInfo{
			ID:         user.ID.Hex(),
			Name:       user.Name,
			UserAgent:  ua,
			RemoteAddr: addr},
	}
	logger.LogInfo(jwtservicedotgo+":71", fmt.Sprintf("User [%s] start a session with [%s]", user.Name, ua))
	// Create token string
	return t.SignedString(signKey)
}

// Verify middleware check client token in header and set uid in request context
func Verify(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := r.Header.Get("Authorization")
		if auth == "" {
			log := fmt.Sprintf("Invalid auth header [%s]", auth)
			logger.LogError(jwtservicedotgo+":93", log)
			response.RespondWithError(w, 401, "Invalid auth")
			return
		}
		token := strings.Split(auth, " ")
		if len(token) != 2 || token[0] != "Bearer" || len(token[1]) < 6 {
			log := fmt.Sprintf("Invalid auth token [%s]", token)
			logger.LogError(jwtservicedotgo+":101", log)
			response.RespondWithError(w, 401, "Invalid auth")
			return
		}
		claim := JWTClaim{}
		data, err := jwt.ParseWithClaims(token[1], &claim, func(token *jwt.Token) (interface{}, error) {
			return verifyKey, nil
		})
		if err != nil || data.Valid == false {
			logger.LogError(jwtservicedotgo+":108", err.Error())
			response.RespondWithError(w, 401, "Bad token, please login")
			return
		}
		context.Set(r, "uid", claim.UserInfo.ID)
		logger.LogTraffic(
			claim.UserInfo.Name,
			r.RequestURI, claim.UserInfo.UserAgent,
			claim.UserInfo.RemoteAddr)
		h.ServeHTTP(w, r)
	})
}
