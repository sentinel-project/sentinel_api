package messaging

import (
	"io/ioutil"
	"net/http"
	"net/url"

	"sentinel/server/models"
	"sentinel/server/services/logger"
)

const (
	// SMSAPI is the free mobile sms api base url
	SMSAPI = "https://smsapi.free-mobile.fr/sendmsg"
	msg    = `

			Hello from sentinel ! 
			      ઊ € $
		
			||             ||
			||   ___ |  __ ||
			|  __0_  |   _0__
			|       |
			|   -------
			   |  ... |
			  . ------   .
			|  
			|            |
			|			 |
			  

		https://image.flaticon.com/icons/svg/910/910311.svg
	`
)

// FreeMobileSEND  send a message by the freemobile api (`WIP`)
func FreeMobileSEND(com models.CommChannel) (string, error) {
	baseURL, err := url.Parse(SMSAPI)
	params := url.Values{}
	params.Add("user", com.Creds["user"])
	params.Add("pass", com.Creds["pass"])
	params.Add("msg", msg)
	baseURL.RawQuery = params.Encode()

	logger.LogInfo("FreeMobileSEND ()", baseURL.String())
	resp, err := http.Get(baseURL.String())
	if err != nil {
		logger.LogError("FreeMobileSEND ()", err.Error())
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	return string(body), err
}
