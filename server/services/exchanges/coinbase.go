package exchanges

import (
	"sentinel/server/models"

	"github.com/BillotP/coinbase"
	cbmodels "github.com/BillotP/coinbase/lib/models"
)

// GetCoinbaseBalances return all coinbase accounts balances
func GetCoinbaseBalances(exchange models.Exchange) ([]cbmodels.Account, error) {
	var err error
	var accounts *cbmodels.Accounts
	client := coinbase.New(&exchange.PublicKey, &exchange.PrivateKey)
	if accounts, err = client.GetAccounts(); err != nil {
		return nil, err
	}
	return accounts.Datas, err
}
