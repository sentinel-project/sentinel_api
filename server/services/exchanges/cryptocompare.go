package exchanges

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"sentinel/server/models"
)

const _cryptocompareurl = "https://min-api.cryptocompare.com/"

// CCPrices correspond to the cryptocompare daily price object
type CCPrices struct {
	Time       int64   `json:"time"`
	Close      float64 `json:"close"`
	High       float64 `json:"high"`
	Low        float64 `json:"low"`
	Open       float64 `json:"open"`
	VolumeFrom float64 `json:"volumefrom"`
	VolumeTo   float64 `json:"volumeto"`
}

// CCResponse correspond to the cryptocompare api response format
type CCResponse struct {
	Response string
	Data     []CCPrices
	TimeTo   int64
	TimeFrom int64
}

// CCLiveEURPrice represent crypto compare api live price resp fmt
type CCLiveEURPrice struct {
	Eur float64 `json:"EUR"`
}

// GetDailyPrices return cryptocompare datas on historics prices for pair
func GetDailyPrices(account models.Exchange, base string, quote string) (CCResponse, error) {
	var requri = _cryptocompareurl + "data/histoday?fsym=" + base + "&tsym=" + quote
	requri += "&limit=10"
	client := &http.Client{}
	r := CCResponse{}

	req, err := http.NewRequest("GET", requri, nil)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	req.Header.Add("Authorization", account.PrivateKey)
	resp, err := client.Do(req)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, &r)
	return r, err
}

// FromBitcoinToEuro return live conversion for btc amount in euro
func FromBitcoinToEuro(account models.Exchange, btc float64) (float64, error) {
	var price = 0.00
	var requri = _cryptocompareurl + "data/price?fsym=" + "BTC" + "&tsyms=" + "EUR"
	client := &http.Client{}
	r := CCLiveEURPrice{}

	req, err := http.NewRequest("GET", requri, nil)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	req.Header.Add("Authorization", "Apikey "+account.PrivateKey)
	resp, err := client.Do(req)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, &r)
	if r.Eur > 0 {
		price = r.Eur * btc
	} else {
		fmt.Printf("%v\n", string(body))
	}
	return price, err
}
