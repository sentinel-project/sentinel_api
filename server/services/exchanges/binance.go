package exchanges

import (
	"context"

	"sentinel/server/models"
	"sentinel/server/services/logger"

	"github.com/adshao/go-binance"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// GetBinanceSymbols return binance exchange  ticker list
func GetBinanceSymbols(exchange models.Exchange) ([]models.Symbols, error) {
	client := binance.NewClient(exchange.PublicKey, exchange.PrivateKey)
	info, err := client.NewExchangeInfoService().Do(context.Background())
	symb := info.Symbols
	eid := exchange.ID.Hex()
	var datas []models.Symbols
	for _, v := range symb {
		datas = append(datas, models.Symbols{
			ID:         primitive.NewObjectID(),
			Symbol:     v.Symbol,
			Base:       v.BaseAsset,
			Quote:      v.QuoteAsset,
			Status:     v.Status,
			ExchangeID: eid,
			OrderTypes: v.OrderTypes,
		})
	}
	if err != nil {
		logger.LogError("GetBinanceSymbols ()", err.Error())
	}
	return datas, err
}

// GetBinanceBalances return user balances on binance exchange
func GetBinanceBalances(exchange models.Exchange) ([]binance.Balance, error) {
	client := binance.NewClient(exchange.PublicKey, exchange.PrivateKey)
	datas, err := client.NewGetAccountService().Do(context.Background())
	if err != nil {
		logger.LogError("GetBinanceBalances ()", err.Error())
		return nil, err
	}
	return datas.Balances, err
}

// GetBinancePrices return live prices from binance exchange
func GetBinancePrices(exchange models.Exchange) ([]models.Prices, error) {
	var fmt []models.Prices
	eid := exchange.ID.Hex()
	client := binance.NewClient(exchange.PublicKey, exchange.PrivateKey)
	datas, err := client.NewListPricesService().Do(context.Background())

	if err != nil {
		logger.LogError("GetBinancePrice ()", err.Error())
	}
	fmt, err = models.ToPricesFromBinance(datas, eid)
	return fmt, err
}

// GetLastBinanceTradesBySymbol return the user trades on binance exchange on symbol
func GetLastBinanceTradesBySymbol(exchange models.Exchange, symbol string) ([]*binance.Order, error) {
	client := binance.NewClient(exchange.PublicKey, exchange.PrivateKey)
	datas, err := client.NewListOrdersService().Symbol(symbol).OrderID(1).Do(context.Background())

	if err != nil {
		logger.LogError("GetLastBinanceTradesBySymbol ()", err.Error())
	}
	return datas, err
}
