package repository

import (
	"context"
	"fmt"
	"log"
	"time"

	"sentinel/server/models"
	"sentinel/server/services/logger"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// CommChannelDAO identify mongo datas
type CommChannelDAO struct {
	Server   string
	Database string
	Client   *mongo.Client
}

var comchandb *mongo.Collection

// COMCHANNELS is the mongoose collection name
const (
	comChannelCollectionName = "messages_gateways"
)

// CommChannelsConnect establish a connection to database
func (m *CommChannelDAO) CommChannelsConnect() {
	comchandb = m.Client.Database(m.Database).Collection(comChannelCollectionName)
	for _, key := range []string{"name", "creds"} {
		indexName, err := userdb.Indexes().CreateOne(
			context.Background(),
			mongo.IndexModel{
				Keys:    bsonx.Doc{{Key: key, Value: bsonx.Int32(1)}},
				Options: options.Index().SetUnique(true),
			},
		)
		if err != nil {
			logger.LogError("BalanceConnect ()", err.Error())
		}
		logger.LogInfo("BalanceConnect ()", fmt.Sprintf("[%s] index setup ok", indexName))
	}
}

// GetUserChannels get all user saved accounts
func (m *CommChannelDAO) GetUserChannels(uid string) ([]models.CommChannel, error) {
	var chans []models.CommChannel
	var c models.CommChannel
	var err error
	// TODO: Check if previous err exist
	fifi := primitive.M{"uid": uid}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// TODO: Mute the previous line code-smell with the muted var
	curr, err := comchandb.Find(ctx, fifi)
	if err != nil {
		log.Fatal(err)
	}
	defer curr.Close(ctx)
	for curr.Next(ctx) {
		err := curr.Decode(&c)
		chans = append(chans, c)
		if err != nil {
			log.Fatal(err)
		}

	}
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return chans, err
}

// InsertChannel into database
func (m *CommChannelDAO) InsertChannel(data models.CommChannel) error {
	data.ID = primitive.NewObjectID()
	res, err := m.Client.Database(m.Database).Collection(comChannelCollectionName).InsertOne(context.Background(), &data)
	if err == nil {
		fmt.Printf("res ? %v\n", res.InsertedID.(primitive.ObjectID).Hex())
		logger.LogInfo("Insert ()", fmt.Sprintf("New account creation for [%s] @ [%s]", data.UserID, data.Name))
	} else {
		logger.LogError("Insert ()", fmt.Sprintf("Account creation error for [%s]", data.UserID))
	}
	return err
}
