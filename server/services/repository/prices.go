package repository

import (
	"context"
	"fmt"
	"log"
	"time"

	"sentinel/server/models"
	"sentinel/server/services/logger"

	"go.mongodb.org/mongo-driver/bson"

	"go.mongodb.org/mongo-driver/mongo"
)

// PricesDAO identify mongo db host and collection
type PricesDAO struct {
	Server   string
	Database string
	Client   *mongo.Client
}

var pricesdb *mongo.Collection

// PricesCollectionName is the mongoose collection name
const (
	PricesCollectionName = "prices"
)

// PricesConnect establish a connection to database
func (m *PricesDAO) PricesConnect() {
	pricesdb = m.Client.Database(m.Database).Collection(PricesCollectionName)
	// TODO: Ensure good indexes !!!
	// indexName, err := pricesdb.Indexes().CreateOne(
	// 	context.Background(),
	// 	mongo.IndexModel{
	// 		Keys: bsonx.Doc{
	// 			{"eid", bsonx.Int32(1)},
	// 			{"date", bsonx.Int32(1)},
	// 			{"price", bsonx.Int32(1)},
	// 			{"symbol", bsonx.Int32(1)}},
	// 		Options: options.Index().SetUnique(true),
	// 	},
	// )
	// if err != nil {
	// 	logger.LogError("PricesConnect ()", err.Error())
	// }
	logger.LogInfo("PricesConnect ()", fmt.Sprintf("[%s] index setup ok", "Prices"))
}

// GetPricesByExchange return all saved prices for a user exchange
func (m *PricesDAO) GetPricesByExchange(eid string) ([]models.Prices, error) {
	var err error
	var price models.Prices
	var prices []models.Prices
	// TODO: Check if previous err exist
	fifi := bson.M{"eid": eid}
	ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
	defer cancel()
	curr, err := pricesdb.Find(ctx, fifi)
	if err != nil {
		log.Fatal(err)
	}
	defer curr.Close(ctx)
	for curr.Next(ctx) {
		err := curr.Decode(&price)
		prices = append(prices, price)
		if err != nil {
			log.Fatal(err)
		}

	}
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return prices, err
}

// InsertPrices insert prices array to db
func (m *PricesDAO) InsertPrices(prices []models.Prices) error {
	var err error
	var contentArray []interface{}
	var res *mongo.InsertManyResult
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)

	defer cancel()
	// Tricks to bulk insert (changing in memory type)
	for _, v := range prices {
		contentArray = append(contentArray, v)
	}
	res, err = pricesdb.InsertMany(ctx, contentArray)
	if err == nil && res != nil {
		logger.LogInfo("InsertPrices ()", fmt.Sprintf("New prices saving for [%s]", prices[0].ExchangeID))
	} else {
		logger.LogError("InsertPrices ()", fmt.Sprintf("Account creation error"))
	}
	return err
}

// GetPricesbySymbol get all prices matching symbol
func (m *PricesDAO) GetPricesbySymbol(eid string, symb string) ([]models.Prices, error) {
	var prices []models.Prices
	var price models.Prices
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	curr, err := pricesdb.Find(ctx, bson.M{"eid": eid, "symbol": symb})
	if err != nil {
		log.Fatal(err)
	}
	defer curr.Close(ctx)
	for curr.Next(ctx) {
		err := curr.Decode(&price)
		prices = append(prices, price)
		if err != nil {
			log.Fatal(err)
		}

	}
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return prices, err
}
