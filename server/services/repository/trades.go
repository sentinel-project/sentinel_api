package repository

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"time"

	"sentinel/server/models"
	"sentinel/server/services/logger"

	"github.com/adshao/go-binance"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// TradesDAO identify mongo db host and collection
type TradesDAO struct {
	Server   string
	Client   *mongo.Client
	Database string
}

var tradesdb *mongo.Collection

// QUOTES is the mongoose collection name
const (
	tradesCollectionName = "trades"
)

// TradesConnect establish a connection to database
func (m *TradesDAO) TradesConnect() {
	tradesdb = m.Client.Database(m.Database).Collection(tradesCollectionName)
	logger.LogInfo("TradesConnect ()", fmt.Sprintf("[%s] index setup ok", "Trades"))
}

// UpdateTrades get trades from binance and save them in db
func (m *TradesDAO) UpdateTrades(eid string, trades []*binance.Order) error {
	var err error
	var ntrades []models.Trade
	var res *mongo.InsertManyResult
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	for _, t := range trades {
		qty, err := strconv.ParseFloat(t.ExecutedQuantity, 64)
		sum, err := strconv.ParseFloat(t.CummulativeQuoteQuantity, 64)
		if err == nil && qty > 0 {
			ntrades = append(ntrades, models.Trade{
				ID:           primitive.NewObjectID(),
				ExchangeID:   eid,
				Symbol:       t.Symbol,
				Quantity:     qty,
				Sum:          sum,
				AveragePrice: sum / qty,
				Type:         string(t.Type),
				Side:         string(t.Side),
				Date:         time.Unix(t.Time/1000, 0).Format(time.RFC3339),
			})
		}
	}
	// Tricks to bulk insert (changing in memory type)
	var contentArray []interface{}
	for _, v := range ntrades {
		contentArray = append(contentArray, v)
	}
	res, err = tradesdb.InsertMany(ctx, contentArray)
	if err == nil && res != nil {
		logger.LogInfo("InsertTrades ()", fmt.Sprintf("New trades saving for [%s]", ntrades[0].ExchangeID))
	} else {
		logger.LogError("InsertTrades ()", fmt.Sprintf("Account creation error"))
	}
	return err
}

// GetTradesByExchange return a user trade by exchange
func (m *TradesDAO) GetTradesByExchange(eid string) ([]models.Trade, error) {
	var err error
	var resp []models.Trade
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	curr, err := tradesdb.Find(ctx, primitive.M{"eid": eid})
	if err != nil {
		log.Fatal(err)
	}
	for curr.Next(context.TODO()) {
		var trade models.Trade
		err := curr.Decode(&trade)
		resp = append(resp, trade)
		if err != nil {
			log.Fatal(err)
		}
	}
	defer curr.Close(context.TODO())
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return resp, err
}

// GetTradesBySymbol return a user trade by exchange
func (m *TradesDAO) GetTradesBySymbol(eid string, symb string) ([]models.Trade, error) {
	var err error
	var resp []models.Trade
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	curr, err := tradesdb.Find(ctx, primitive.M{"eid": eid, "sym": symb})
	if err != nil {
		log.Fatal(err)
	}
	for curr.Next(context.TODO()) {
		var trade models.Trade
		err := curr.Decode(&trade)
		resp = append(resp, trade)
		if err != nil {
			log.Fatal(err)
		}
	}
	defer curr.Close(context.TODO())
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return resp, err
}
