package repository

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"sentinel/server/models"
	"sentinel/server/services/logger"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// BalanceDAO identify mongo db host and collection
type BalanceDAO struct {
	Server   string
	Database string
	Client   *mongo.Client
}

var balancedb *mongo.Collection

// BALANCES is the mongoose collection name
const (
	balanceCollection = "balances"
)

// BalanceConnect establish a connection to database
func (m *BalanceDAO) BalanceConnect() {
	balancedb = m.Client.Database(m.Database).Collection(balanceCollection)
	// TODO Must write a net method to get all keys for all models

	indexName, err := balancedb.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys: bsonx.Doc{
				{Key: "asset", Value: bsonx.Int32(1)},
				{Key: "uid", Value: bsonx.Int32(1)},
				{Key: "eid", Value: bsonx.Int32(1)}},
			Options: options.Index().SetUnique(true),
		},
	)
	if err != nil {
		logger.LogError("BalanceConnect ()", err.Error())
	}
	logger.LogInfo("BalanceConnect ()", fmt.Sprintf("[%s] index setup ok", indexName))
}

// InsertBalances into database
func (m *BalanceDAO) InsertBalances(balances []models.Balance) error {
	//
	var res *mongo.InsertManyResult
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	// Tricks to bulk insert (changing in memory type)
	var contentArray []interface{}
	for _, v := range balances {
		contentArray = append(contentArray, v)
	}
	res, err = balancedb.InsertMany(ctx, contentArray)
	if err == nil && res != nil {
		logger.LogInfo("InsertBalances ()", fmt.Sprintf("New balance saving for [%s]", balances[0].UserID))
	} else {
		logger.LogError("InsertBalances ()", fmt.Sprintf("Account creation error"))
	}
	return err
}

// InsertBalance into database
func (m *BalanceDAO) InsertBalance(balance models.Balance) error {
	//
	var res *mongo.InsertOneResult
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	res, err = balancedb.InsertOne(ctx, &balance)
	if err == nil && res != nil {
		logger.LogInfo("InsertBalances ()", fmt.Sprintf("New balance saving for [%s]", balance.UserID))
	} else {
		logger.LogError("InsertBalances ()", fmt.Sprintf("Account creation error"))
	}
	return err
}

// UpdateBalance into database
func (m *BalanceDAO) UpdateBalance(balance models.Balance) error {
	//
	var res *mongo.SingleResult
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	fifi := primitive.M{"_id": balance.ID}
	res = balancedb.FindOneAndReplace(ctx, fifi, &balance)
	if err == nil && res != nil {
		logger.LogInfo("InsertBalances ()", fmt.Sprintf("New balance saving for [%s]", balance.UserID))
	} else {
		logger.LogError("InsertBalances ()", fmt.Sprintf("Account creation error"))
	}
	return err
}

// GetUserBalances return alls balances for a user id
func (m *BalanceDAO) GetUserBalances(uid string) ([]models.Balance, error) {
	var err error
	var bals []models.Balance

	ctx, cancel := context.WithTimeout(context.TODO(), 10*time.Second)
	defer cancel()
	curr, err := balancedb.Find(ctx, primitive.M{"uid": uid})
	if err != nil {
		log.Fatal(err)
	}
	for curr.Next(context.TODO()) {
		var balHolder models.Balance
		err := curr.Decode(&balHolder)
		bals = append(bals, balHolder)
		if err != nil {
			log.Fatal(err)
		}
	}
	defer curr.Close(context.TODO())
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return bals, err
}

// GetBalancesByEID return alls balances for a exchange id
func (m *BalanceDAO) GetBalancesByEID(eid string) ([]models.Balance, error) {
	var err error
	var bals []models.Balance

	ctx, cancel := context.WithTimeout(context.TODO(), 10*time.Second)
	defer cancel()
	curr, err := balancedb.Find(ctx, primitive.M{"eid": eid})
	if err != nil {
		log.Fatal(err)
	}
	for curr.Next(context.TODO()) {
		var balHolder models.Balance
		err := curr.Decode(&balHolder)
		bals = append(bals, balHolder)
		if err != nil {
			log.Fatal(err)
		}
	}
	defer curr.Close(context.TODO())
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return bals, err
}

// UpdateBalances update balances in db
func (m *BalanceDAO) UpdateBalances(nbal []models.Balance) error {
	// var res *mongo.InsertManyResult
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err = balancedb.DeleteMany(ctx, primitive.M{"uid": nbal[0].UserID})
	if err != nil {
		logger.LogError("UpdateBalances ()", fmt.Sprintf("Account creation error %v", err.Error()))
		return err
	}
	// Tricks to bulk insert (changing in memory type)
	var contentArray []interface{}
	for _, v := range nbal {
		contentArray = append(contentArray, v)
	}
	_, err = balancedb.InsertMany(ctx, contentArray)
	if err != nil {
		logger.LogError("UpdateBalances ()", fmt.Sprintf("Account creation error %v", err.Error()))
		return err
	}
	logger.LogInfo("UpdateBalances ()", fmt.Sprintf("Account creation sucess %v", nbal[0].UserID))
	return err
}

// contains check if string e is in strings slice s
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// SetMarkets set a bal.Markets symbols array according to a markets symbol slice
func (m *BalanceDAO) SetMarkets(bal models.Balance, mrkts []string) models.Balance {
	for _, tsym := range mrkts {
		if strings.HasPrefix(tsym, bal.Asset) &&
			contains(bal.Markets, tsym) == false {
			bal.Markets = append(bal.Markets, tsym)
		}
	}
	return bal
}
