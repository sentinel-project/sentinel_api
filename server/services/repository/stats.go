package repository

import (
	"fmt"

	"sentinel/server/services/logger"

	"go.mongodb.org/mongo-driver/mongo"
)

// StatsDA0 identify the mongo db host and collection
type StatsDA0 struct {
	Server   string
	Database string
	Client   *mongo.Client
}

var statsdb *mongo.Collection

// StatsCollectionName represent stat collection name
const (
	StatsCollectionName = "stats"
)

// StatsConnect establish a connection to database
func (m *StatsDA0) StatsConnect() {
	statsdb = m.Client.Database(m.Database).Collection(StatsCollectionName)
	// TODO: ensure consistent indexes !!!
	logger.LogInfo("StatsConnect ()", fmt.Sprintf("[%s] index setup ok", "Stats"))
}

// // InsertStats insert stats datas into db
// func (m *StatsDA0) InsertStats(nstat models.Stat) {
// 	var err error
// 	var res *mongo.InsertManyResult
// 	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
// 	defer cancel()

// }
