package repository

import (
	"context"
	"fmt"
	"log"
	"time"

	"sentinel/server/models"
	"sentinel/server/services/logger"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

// ExchangeDAO identify mongo db host and collection
type ExchangeDAO struct {
	Server   string
	Database string
	Client   *mongo.Client
}

var exchangedb *mongo.Collection

// EXCHANGES is the mongoose collection name
const (
	exchangeCollectionName = "exchanges"
)

// ExchangeConnect establish a connection to database
func (m *ExchangeDAO) ExchangeConnect() {
	exchangedb = m.Client.Database(m.Database).Collection(exchangeCollectionName)
	// TODO Must write a net method to get all keys for all models
	for _, key := range []string{"publickey", "privatekey"} {
		indexName, err := exchangedb.Indexes().CreateOne(
			context.Background(),
			mongo.IndexModel{
				Keys:    bsonx.Doc{{Key: key, Value: bsonx.Int32(1)}},
				Options: options.Index().SetUnique(true),
			},
		)
		if err != nil {
			// logger.LogError("UsersConnect ()", err.Error())
			break
		}
		logger.LogInfo("ExchangeConnect ()", fmt.Sprintf("[%s] index setup ok", indexName))
	}
}

// GetAllAccounts find list of all users api accounts
func (m *ExchangeDAO) GetAllAccounts() ([]models.Exchange, error) {
	var exchanges []models.Exchange
	ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
	defer cancel()
	curr, err := exchangedb.Find(ctx, bson.M{})
	defer curr.Close(ctx)
	for curr.Next(ctx) {
		err := curr.Decode(&exchanges)
		if err != nil {
			log.Fatal(err)
		}

	}
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return exchanges, err
}

// FindUserAccounts find accounts for a unique user id
func (m *ExchangeDAO) FindUserAccounts(uid string) ([]models.Exchange, error) {
	var account models.Exchange
	var accounts []models.Exchange
	var err error

	filter := bson.M{"uid": uid}
	ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
	defer cancel()
	curr, err := exchangedb.Find(ctx, filter)
	if err != nil {
		log.Fatal(err)
	}
	defer curr.Close(ctx)
	for curr.Next(ctx) {
		err := curr.Decode(&account)
		accounts = append(accounts, account)
		if err != nil {
			log.Fatal(err)
		}

	}
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return accounts, err
}

// InsertExchange into database
func (m *ExchangeDAO) InsertExchange(data models.Exchange) error {
	data.ID = primitive.NewObjectID()
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	res, err := exchangedb.InsertOne(ctx, &data)
	if err == nil && res != nil {
		logger.LogInfo("InsertExchange()", fmt.Sprintf("New account creation for [%s] @ [%s]", data.UserID, data.Name))
	} else {
		logger.LogError("InsertExchange()", fmt.Sprintf("Account creation error for [%s]", data.UserID))
	}
	return err
}

// Delete an existing exchange
func (m *ExchangeDAO) Delete(data models.Exchange) error {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := exchangedb.DeleteOne(ctx, &data)
	return err
}

// Update an existing data object
func (m *ExchangeDAO) Update(data models.Exchange) error {
	var err error
	filter := bson.M{"_id": data.ID}
	update := bson.M{
		"$set": bson.M{
			"pubkey":  data.PublicKey,
			"privkey": data.PrivateKey,
		},
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	resp := exchangedb.FindOneAndUpdate(ctx, filter, update)
	if resp != nil {
		msg := fmt.Sprintf("Updated account [%v]", data.ID)
		logger.LogInfo("UpdateExchange()", msg)
	}
	return err
}

// GetUserAccount find and return a named exchange for an uid
func (m *ExchangeDAO) GetUserAccount(name string, uid string) (models.Exchange, error) {
	var account models.Exchange
	var err error
	filter := bson.M{"uid": uid, "name": name}
	// user.Password =
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()
	resp := exchangedb.FindOne(ctx, filter)
	resp.Decode(&account)
	return account, err
}
