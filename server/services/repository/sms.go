package repository

import (
	"context"
	"errors"
	"fmt"
	"log"

	"sentinel/server/models"
	"sentinel/server/services/logger"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// SmsDAO identify mongo db host and collection
type SmsDAO struct {
	Client   *mongo.Client
	Server   string
	Database string
}

var smsdb *mongo.Collection

const (
	// SMS is the mongoose collection name.
	SMS = "sms"
)

// SmsConnect establish a connection to database
func (m *SmsDAO) SmsConnect() {
	smsdb = m.Client.Database(m.Database).Collection(SMS)
}

// GetSmsByID get the sms id element
func (m *SmsDAO) GetSmsByID(sid string) (models.Sms, error) {
	var tosend models.Sms
	var err error
	obj, err := primitive.ObjectIDFromHex(sid)
	if err != nil {
		// err = smsdb.C(SMS).FindId(obj).One(&tosend)
		err := smsdb.FindOne(context.Background(), primitive.M{"_id": obj}).Decode(&tosend)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		err = errors.New("No such sms")
	}
	return tosend, err
}

// InsertMany messages into sms database
func (m *SmsDAO) InsertMany(messages []models.Sms) error {
	var contentArray []interface{}
	for _, v := range messages {
		contentArray = append(contentArray, v)
	}
	_, err := smsdb.InsertMany(context.Background(), contentArray)
	if err == nil {
		logger.LogInfo("Insert ()", fmt.Sprintf("[%v] messages successfully saved", len(messages)))
	} else {
		logger.LogError("Insert ()", fmt.Sprintf("Failed to send messages"))
	}
	return err
}
