package repository

import (
	"context"
	"fmt"
	"log"
	"time"

	"sentinel/server/models"
	"sentinel/server/services/logger"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"golang.org/x/crypto/bcrypt"
)

// UsersDAO identify mongo userdb host and collection
type UsersDAO struct {
	Server   string
	Database string
	Client   *mongo.Client
}

var userdb *mongo.Collection

// COLLECTION is the mongoose collection name
const (
	userCollectionName = "users"
)

// Connect establish a connection to database
func (m *UsersDAO) Connect() {
	userdb = m.Client.Database(m.Database).Collection(userCollectionName)

	// TODO Must write a net method to get all keys for all models

	for _, key := range []string{"name", "email"} {
		indexName, err := userdb.Indexes().CreateOne(
			context.Background(),
			mongo.IndexModel{
				Keys:    bsonx.Doc{{Key: key, Value: bsonx.Int32(1)}},
				Options: options.Index().SetUnique(true),
			},
		)
		if err != nil {
			logger.LogError("UsersConnect ()", err.Error())
		}
		logger.LogInfo("Connect Index ()", fmt.Sprintf("[%s] index setup ok", indexName))
	}
}

func hashAndSalt(pwd []byte) string {
	// Use GenerateFromPassword to hash & salt pwd.
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword(pwd, 5)
	if err != nil {
		logger.LogError("hashAndSalt ()", err.Error())
		return ""
	} // GenerateFromPassword returns a byte slice so we need to
	// convert the bytes to a string and return it
	return string(hash)
}

// ComparePasswords check if plainPwd match the stored bcrypt hash
func ComparePasswords(hashedPwd string, plainPwd []byte) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		logger.LogError("comparePassword ()", err.Error())
		return false
	}
	return true
}

// FindAll find list of users
func (m *UsersDAO) FindAll() ([]models.User, error) {
	var users []models.User
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()
	curr, err := userdb.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	defer curr.Close(ctx)
	for curr.Next(ctx) {
		err := curr.Decode(&users)
		if err != nil {
			log.Fatal(err)
		}

	}
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return users, err
}

// FindByID find a user by its id
func (m *UsersDAO) FindByID(id string) (models.User, error) {
	var user models.User
	var err error
	filter, err := primitive.ObjectIDFromHex(id)
	// TODO: Check if previous err exist
	fifi := bson.M{"_id": filter}
	ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
	defer cancel()
	// TODO: Mute the previous line code-smell with the muted var
	err = userdb.FindOne(ctx, fifi).Decode(&user)
	return user, err
}

// FindByName find a user by its name
func (m *UsersDAO) FindByName(name string) (models.User, error) {
	var user models.User
	var err error
	filter := bson.M{"name": name}
	ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
	defer cancel()
	err = userdb.FindOne(ctx, filter).Decode(&user)
	return user, err
}

// Insert a user into database
func (m *UsersDAO) Insert(user models.User) error {
	user.ID = primitive.NewObjectID()
	user.Password = hashAndSalt([]byte(user.Password))
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	// Hashing password
	_, err := userdb.InsertOne(ctx, &user)
	if err == nil {
		logger.LogInfo("Insert ()", fmt.Sprintf("New account creation for [%s] @ [%s]", user.Name, user.Email))
	} else {
		logger.LogError("Insert ()", fmt.Sprintf("Account creation error for [%s]", user.Email))
	}
	return err
}

// Delete an existing user
func (m *UsersDAO) Delete(user models.User) error {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := userdb.DeleteOne(ctx, &user)
	return err
}

/// TODO: FIX password update !!!

// Update an existing user
func (m *UsersDAO) Update(uid string, user models.User) error {
	var err error
	user.ID, err = primitive.ObjectIDFromHex(uid)
	filter := bson.M{"_id": user.ID}
	update := bson.M{
		"$set": bson.M{
			"email":       user.Email,
			"cover_image": user.CoverImage,
		},
	}
	// user.Password =
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	resp := userdb.FindOneAndUpdate(ctx, filter, update)
	if resp != nil {
		msg := fmt.Sprintf("Updated user [%v]", uid)
		logger.LogInfo("Update(user) ()", msg)
	}
	return err
}
