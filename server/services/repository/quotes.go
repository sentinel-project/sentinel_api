package repository

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"sentinel/server/models"
	"sentinel/server/services/logger"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// QuotesDAO identify mongo db host and collection
type QuotesDAO struct {
	Server   string
	Client   *mongo.Client
	Database string
}

var quotesdb *mongo.Collection

// QUOTES is the mongoose collection name
const (
	quotesCollectionName = "quotes"
)

// QuotesConnect establish a connection to database
func (m *QuotesDAO) QuotesConnect() {
	quotesdb = m.Client.Database(m.Database).Collection(quotesCollectionName)
	// bibi, err := bson.Marshal([]string{"eid", "value", "pair"})
	// indexName, err := quotesdb.Indexes().CreateOne(
	// 	context.Background(),
	// 	mongo.IndexModel{
	// 		Keys:    bibi,
	// 		Options: options.Index().SetUnique(true),
	// 	},
	// )
	// if err != nil {
	// 	logger.LogError("QuotesConnect ()", err.Error())
	// }
	logger.LogInfo("QuotesConnect ()", fmt.Sprintf("[%s] index setup ok", "Quotes"))
}

// GetQuotesByExchangeID return a user recorded quoting prices
func (m *QuotesDAO) GetQuotesByExchangeID(eid string) ([]models.Quotes, error) {
	var err error
	var quotes []models.Quotes
	var quote models.Quotes
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	curr, err := quotesdb.Find(ctx, primitive.M{"eid": eid})
	if err != nil {
		log.Fatal(err)
	}
	for curr.Next(context.TODO()) {
		err := curr.Decode(&quote)
		quotes = append(quotes, quote)
		if err != nil {
			log.Fatal(err)
		}

	}
	defer curr.Close(context.TODO())
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return quotes, err
}

// GetQuotesByUserID return a user recorded quoting prices
func (m *QuotesDAO) GetQuotesByUserID(uid string) ([]models.Quotes, error) {
	var err error
	var elems []models.Quotes
	var el models.Quotes
	ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
	defer cancel()
	curr, err := quotesdb.Find(ctx, primitive.M{"uid": uid})
	if err != nil {
		log.Fatal(err)
	}
	defer curr.Close(ctx)
	for curr.Next(ctx) {
		err := curr.Decode(&el)
		elems = append(elems, el)
		if err != nil {
			log.Fatal(err)
		}

	}
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return elems, err
}

// GetQuotesByBalanceID return a user recorded quoting prices per one balance
func (m *QuotesDAO) GetQuotesByBalanceID(aid string) ([]models.Quotes, error) {
	var elems []models.Quotes
	var el models.Quotes
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	curr, err := quotesdb.Find(ctx, primitive.M{"aid": aid})
	if err != nil {
		log.Fatal(err)
	}
	defer curr.Close(ctx)
	for curr.Next(ctx) {
		err := curr.Decode(&el)
		elems = append(elems, el)
		if err != nil {
			log.Fatal(err)
		}

	}
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return elems, err
}

// GetQuotesByPair return a user (assetId) quotes filered by market symbol
func (m *QuotesDAO) GetQuotesByPair(pair string, aid string) ([]models.Quotes, error) {
	var elems []models.Quotes
	var el models.Quotes
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	curr, err := quotesdb.Find(ctx, primitive.M{"aid": aid, "pair": pair})
	if err != nil {
		log.Fatal(err)
	}
	defer curr.Close(ctx)
	for curr.Next(ctx) {
		err := curr.Decode(&el)
		elems = append(elems, el)
		if err != nil {
			log.Fatal(err)
		}

	}
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return elems, err
}

// GetQuotesBySymbol return a user (exchangeID) quotes filered by market symbol
func (m *QuotesDAO) GetQuotesBySymbol(eid string, symb string) ([]models.Quotes, error) {
	var elems []models.Quotes
	var el models.Quotes
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	curr, err := quotesdb.Find(ctx, primitive.M{"eid": eid, "pair": symb})
	if err != nil {
		log.Fatal(err)
	}
	defer curr.Close(ctx)
	for curr.Next(ctx) {
		err := curr.Decode(&el)
		elems = append(elems, el)
		if err != nil {
			log.Fatal(err)
		}

	}
	if err := curr.Err(); err != nil {
		log.Fatal(err)
	}
	return elems, err
}

// CalcQuotes for each asset in user balance
func (m *QuotesDAO) CalcQuotes(bals []models.Balance, prices []models.Prices) ([]models.Quotes, error) {
	var err error
	var filt []models.Quotes
	if bals == nil || prices == nil || len(bals) == 0 || len(prices) == 0 {
		err = errors.New("Invalid prices or balances")
	} else {
		for bindex, v := range bals {
			for mindex := range v.Markets {
				for pindex, p := range prices {
					if p.Symbol == bals[bindex].Markets[mindex] {
						filt = append(filt, models.Quotes{
							ID:         primitive.NewObjectID(),
							UserID:     bals[bindex].UserID,
							Pair:       bals[bindex].Markets[mindex],
							Date:       prices[pindex].Date,
							AssetID:    bals[bindex].ID.Hex(),
							ExchangeID: bals[bindex].ExchangeID,
							Value:      (bals[bindex].Free + bals[bindex].Locked) * prices[pindex].Price,
						})
					}
				}
			}
		}
	}
	return filt, err
}

// InsertQuotes save newly computed quotes
func (m *QuotesDAO) InsertQuotes(update bool, nquotes []models.Quotes) error {
	var res *mongo.InsertManyResult
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if update == true {
		fifi := primitive.M{"uid": nquotes[0].UserID}
		_, err := quotesdb.DeleteMany(ctx, fifi)
		if err != nil {
			return err
		}
	}
	var contentArray []interface{}
	for _, v := range nquotes {
		contentArray = append(contentArray, v)
	}
	res, err = quotesdb.InsertMany(ctx, contentArray)
	if err == nil && res != nil {
		logger.LogInfo("InsertQuotes ()", fmt.Sprintf("New quotes saving for [%s]", nquotes[0].ExchangeID))
	} else {
		logger.LogError("InsertQuotes ()", fmt.Sprintf("Account creation error"))
	}
	return err
}
