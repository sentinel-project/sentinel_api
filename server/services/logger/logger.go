package logger

import (
	"fmt"
	"net/http"
	"os"
	"time"
)

// ReadUserIP get correct oip for client request
func ReadUserIP(r *http.Request) string {
	IPAddress := r.Header.Get("X-Real-Ip")
	if IPAddress == "" {
		IPAddress = r.Header.Get("X-Forwarded-For")
	}
	if IPAddress == "" {
		IPAddress = r.RemoteAddr
	}
	return IPAddress
}

// LogError format **error** messages on standard error output
func LogError(origin string, msg string) {
	symb := []byte("🌩")
	now := time.Now().Format(time.RFC3339)
	fmt.Fprintf(os.Stderr, "[%s] %s  Error [%s]: %s\n", now, symb, origin, msg)
}

// LogInfo format **info** messages on standard output
func LogInfo(origin string, msg string) {
	symb := []byte("👉")
	now := time.Now().Format(time.RFC3339)
	fmt.Printf("[%s] %s  Info [%s]: %s\n", now, symb, origin, msg)
}

// LogTraffic print api requests information on standard output
func LogTraffic(username string, request string, ua string, from string) {
	msgfmt := "User [%s] is passing by [%s] with [%s] from [%s]"
	msg := fmt.Sprintf(msgfmt, username, request, ua, from)
	LogInfo("Traffic!", msg)
}
