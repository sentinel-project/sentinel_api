package statistics

import (
	"fmt"
	"sort"
	"strings"

	"sentinel/server/models"
)

func minMax(array []models.Sum) (models.Sum, models.Sum) {
	var max = array[0]
	var min = array[0]
	for _, value := range array {
		if max.Value < value.Value {
			max = value
		}
		if min.Value > value.Value {
			min = value
		}
	}
	return min, max
}

// GetStats returns the total in one currency from all asset valuation
func GetStats(bals []models.Balance, quotes []models.Quotes) (models.Stat, error) {
	var stats models.Stat
	var sums []models.Sum
	var sum models.Sum
	var err error
	var totbase = 0.00

	sum = models.Sum{Base: "BTC", Value: 0}
	for _, b := range bals {
		if b.Asset == "BTC" {
			totbase += b.Free
			continue
		} else {
			var fifi []models.Quotes
			for i, c := range quotes {
				if c.AssetID == b.ID.Hex() &&
					strings.Contains(c.Pair, "BTC") {
					fifi = append(fifi, quotes[i])
				}
			}
			sums = append(sums, models.Sum{
				Base:  "BTC" + "-" + b.Asset,
				Value: fifi[len(fifi)-1].Value,
			})
			sum.Value += fifi[len(fifi)-1].Value
		}
	}
	sum.Value += totbase
	min, max := minMax(sums)
	mmm := []models.Sum{min, max}
	sort.Slice(sums, func(i, j int) bool { return sums[i].Value > sums[j].Value })
	stats = models.Stat{
		Sum:    sum,
		MinMax: mmm,
		Ranks:  sums,
		UserID: bals[0].UserID,
	}
	return stats, err
}

// GetEvoStats return quotes evolution on all user balances
func GetEvoStats(bals []models.Balance, quotes []models.Quotes) (models.Stat, error) {
	var resp models.Stat
	var evos []models.Sum
	var mrkts float64
	var err error
	// Real calculus ops (percentage evo) over each user asset
	for _, b := range bals {
		var bquotes []models.Quotes
		for _, c := range quotes {
			// WARN: Evo are on <ASSETNAME>BTC pairs only !
			if c.AssetID == b.ID.Hex() &&
				strings.Contains(c.Pair, "BTC") {
				bquotes = append(bquotes, c)
				mrkts++
			}
		}
		evo := (bquotes[len(bquotes)-1].Value - bquotes[0].Value)
		perc := (evo / bquotes[0].Value) * 100
		evos = append(evos, models.Sum{
			Base:  bquotes[0].Pair,
			Value: perc,
		})
	}
	// And that's just format
	min, max := minMax(evos)
	minmax := []models.Sum{min, max}
	// Ordering evos from greatest to lowest
	sort.Slice(evos, func(i, j int) bool { return evos[i].Value > evos[j].Value })
	resp = models.Stat{
		Sum: models.Sum{
			Base:  "quotes",
			Value: mrkts,
		},
		MinMax: minmax,
		Ranks:  evos,
		UserID: bals[0].UserID,
	}
	return resp, err
}

// GetTradingStatsBySymbol return user stats on his trades for symbol
func GetTradingStatsBySymbol(trades []models.Trade, prices []models.Prices) (models.TradeStat, error) {
	var err error
	var totalQtt = 0.0
	var totalAvgPrice = 0.0
	var sym = trades[0].Symbol
	var lastprices = prices[len(prices)-1].Price
	var lasttradeprice = trades[len(trades)-1].AveragePrice

	evo := lastprices - lasttradeprice
	perc := (evo / lasttradeprice) * 100
	// TODO: fix totalQtt with deposit / withdrawal or final balance
	for _, t := range trades {
		if t.Side == "BUY" {
			totalQtt += t.Quantity
			totalAvgPrice += t.AveragePrice * t.Quantity
		} else if t.Side == "SELL" {
			totalQtt -= t.Quantity
		}
	}
	vwagevo := (lastprices - (totalAvgPrice / totalQtt))
	vwagperc := (vwagevo / (totalAvgPrice / totalQtt)) * 100
	var tradeadvice = ""
	if vwagperc <= 20 {
		tradeadvice = "BUY"
	} else {
		tradeadvice = "SELL"
	}
	fmt.Printf("Qtt : %v\n", totalQtt)
	return models.TradeStat{
		Symbol:                     sym,
		FromLastTrade:              perc,
		FromVolumeWAP:              vwagperc,
		VolumeWeightedAveragePrice: totalAvgPrice / totalQtt,
		Sentiment:                  tradeadvice,
		LastPriceID:                prices[len(prices)-1].ID.Hex(),
		LastTradePriceID:           trades[len(trades)-1].ID.Hex(),
	}, err
}
