package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// Symbols represent the tradable markets on an particular exchange
type Symbols struct {
	ID         primitive.ObjectID `bson:"_id" json:"-"`
	Symbol     string             `bson:"symbol" json:"symbol"`
	Base       string             `bson:"base" json:"base"`
	Quote      string             `bson:"quote" json:"quote"`
	Status     string             `bson:"status" json:"status"`
	ExchangeID string             `bson:"eid" json:"-"`
	OrderTypes []string           `bson:"ordertypes" json:"ordertypes"`
}
