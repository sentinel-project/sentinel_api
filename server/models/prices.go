package models

import (
	"strconv"
	"time"

	"sentinel/server/services/logger"

	"github.com/adshao/go-binance"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Prices represent a user portfolio live prices
type Prices struct {
	ID         primitive.ObjectID `bson:"_id" json:"-"`
	Symbol     string             `bson:"symbol" json:"symbol"`
	ExchangeID string             `bson:"eid" json:"-"`
	Price      float64            `bson:"price" json:"price"`
	Date       string             `bson:"date" json:"date"`
}

// ToPricesFromBinance do conversion to Price object
func ToPricesFromBinance(datas []*binance.SymbolPrice, eid string) ([]Prices, error) {
	var fmt []Prices
	var err error
	var price float64
	var now = time.Now().Format(time.RFC3339)

	for _, v := range datas {
		price, err = strconv.ParseFloat(v.Price, 64)
		if err == nil {
			fmt = append(fmt, Prices{
				ID:         primitive.NewObjectID(),
				Symbol:     v.Symbol,
				Price:      price,
				ExchangeID: eid,
				Date:       now,
			})
		} else {
			logger.LogError("GetBinancePrice ()", err.Error())
			break
		}
	}
	return fmt, err
}
