package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// User represents a user, we uses bson keyword to tell the mgo driver how to name
// the properties in mongodb document
type User struct {
	ID         primitive.ObjectID `bson:"_id" json:"-"`
	Name       string             `bson:"name" json:"name"`
	CoverImage string             `bson:"cover_image" json:"cover_image"`
	Email      string             `bson:"email" json:"email"`
	Password   string             `bson:"password" json:"password"`
}

// NewUser create a user with required param
func NewUser(name string, email string, password string, coverImage string) User {
	return User{
		ID:         primitive.NewObjectID(),
		Name:       name,
		CoverImage: coverImage,
		Email:      email,
		Password:   password,
	}
}

// NewUserFromObject return a new user with valid fields
func NewUserFromObject(data User) User {

	return User{
		ID:         primitive.NewObjectID(),
		Name:       data.Name,
		CoverImage: data.CoverImage,
		Email:      data.Email,
		Password:   data.Password,
	}
}
