package models

// Sum represent sum for a user portfolio in one base currency
type Sum struct {
	Base  string  `bson:"base" json:"base"`
	Value float64 `bson:"value" json:"value"`
}

// Evo represent an asset evolution percentage
type Evo struct {
	Pair  string  `bson:"pair" json:"pair"`
	Value float64 `bson:"val" json:"value"`
}

// Stat gather the global stats availables
type Stat struct {
	Sum    Sum    `bson:"sum" json:"sum"`
	MinMax []Sum  `bson:"minmax" json:"minmax"`
	Ranks  []Sum  `bson:"ranks" json:"ranks"`
	UserID string `bson:"uid" json:"-"`
}

// TradeStat represent a trade stats for symbol
type TradeStat struct {
	Symbol                     string  `bson:"sym" json:"symbol"`
	FromLastTrade              float64 `bson:"from_ltrade" json:"from_lasttrade"`
	FromVolumeWAP              float64 `bson:"from_wap" json:"from_vwap"`
	VolumeWeightedAveragePrice float64 `bson:"vwap" json:"vwap"`
	LastPriceID                string  `bson:"pid" json:"-"`
	LastTradePriceID           string  `bson:"tid" json:"-"`
	Sentiment                  string  `bson:"sent" json:"sentiment"`
}
