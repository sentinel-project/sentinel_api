package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// Exchange represents a user api credentials object in database
type Exchange struct {
	ID         primitive.ObjectID `bson:"_id" json:"-"`
	Name       string             `bson:"name" json:"name"`
	UserID     string             `bson:"uid" json:"-"`
	PublicKey  string             `bson:"publickey" json:"pubkey"`
	PrivateKey string             `bson:"privatekey" json:"privkey"`
}
