package models

// UserInfo define some custom types were going to use within our tokens
type UserInfo struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	UserAgent  string `json:"ua"`
	RemoteAddr string `json:"ip"`
}
