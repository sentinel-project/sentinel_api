package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// CommChannel represent a user various messaging channels credentials
type CommChannel struct {
	ID     primitive.ObjectID `bson:"_id" json:"-"`
	UserID string             `bson:"uid" json:"-"`
	Name   string             `bson:"name" json:"name"`
	Creds  map[string]string  `bson:"creds" json:"creds"`
}
