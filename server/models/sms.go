package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// Sms is a sms storage model
type Sms struct {
	ID     primitive.ObjectID `bson:"_id" json:"-"`
	Body   string             `bson:"body" json:"body"`
	UserID string             `bson:"uid" json:"-"`
	SendBy string             `bson:"sender" json:"sender"`
	SendAt string             `bson:"datesent" json:"datesent"`
}
