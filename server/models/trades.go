package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// Trade represent a user trade object
type Trade struct {
	ID           primitive.ObjectID `bson:"_id" json:"-"`
	Symbol       string             `bson:"sym" json:"symbol"`
	Quantity     float64            `bson:"qtt" json:"quantity"`
	Sum          float64            `bson:"sum" json:"sum"`
	AveragePrice float64            `bson:"avg_price" json:"avg_price"`
	Type         string             `bson:"type" json:"type"`
	Side         string             `bson:"side" json:"side"`
	Date         string             `bson:"date" json:"date"`
	ExchangeID   string             `bson:"eid" json:"-"`
}
