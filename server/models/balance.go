package models

import (
	"strconv"
	"time"

	cbmodels "github.com/BillotP/coinbase/lib/models"
	"github.com/adshao/go-binance"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Balance represent a user balance object in database
type Balance struct {
	ID         primitive.ObjectID `bson:"_id" json:"-"`
	Asset      string             `bson:"asset" json:"asset"`
	Free       float64            `bson:"free" json:"free"`
	Locked     float64            `bson:"locked" json:"locked"`
	Markets    []string           `bson:"markets" json:"markets"`
	UserID     string             `bson:"uid" json:"-"`
	ExchangeID string             `bson:"eid" json:"-"`
	UpdatedAt  string             `bson:"lastupdate" json:"lastupdate"`
}

// FromBinanceToBalance serialize binance api data to our own Balance format
func FromBinanceToBalance(bals []binance.Balance, eid string, uid string) ([]Balance, error) {
	var err error
	var free float64
	var locked float64
	var filt []Balance
	var now = time.Now().Format(time.RFC3339)
	for _, v := range bals {
		free, err = strconv.ParseFloat(v.Free, 64)
		locked, err = strconv.ParseFloat(v.Locked, 64)
		if (free > 0 || locked > 0) && err == nil {
			filt = append(filt, Balance{
				ID:         primitive.NewObjectID(),
				Asset:      v.Asset,
				Free:       free,
				Locked:     locked,
				Markets:    nil,
				UserID:     uid,
				ExchangeID: eid,
				UpdatedAt:  now,
			})
		}
	}
	return filt, err
}

// FromCoinbaseToBalance serialize coinbase api data to our own Balance format
func FromCoinbaseToBalance(bals []cbmodels.Account, eid string, uid string) ([]Balance, error) {
	var err error
	var free float64
	var locked float64
	var filt []Balance
	var now = time.Now().Format(time.RFC3339)
	for _, v := range bals {
		free, err = strconv.ParseFloat(v.Balance.Amount, 64)
		// locked, err = strconv.ParseFloat(v.Locked, 64)
		if (free > 0 || locked > 0) && err == nil {
			filt = append(filt, Balance{
				ID:         primitive.NewObjectID(),
				Asset:      v.Balance.Currency,
				Free:       free,
				Locked:     locked,
				Markets:    nil,
				UserID:     uid,
				ExchangeID: eid,
				UpdatedAt:  now,
			})
		}
	}
	return filt, err
}
