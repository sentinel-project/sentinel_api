package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// Quotes represent a user asset value
type Quotes struct {
	ID         primitive.ObjectID `bson:"_id" json:"-"`
	UserID     string             `bson:"uid" json:"-"`
	Pair       string             `bson:"pair" json:"pair"`
	Value      float64            `bson:"value" json:"value"`
	AssetID    string             `bson:"aid" json:"-"`
	ExchangeID string             `bson:"eid" json:"-"`
	Date       string             `bson:"date" json:"date"`
}
