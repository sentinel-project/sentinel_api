# 💂 Sentinel
**A versatile crypto trading toolset**

[![Generic badge](https://img.shields.io/badge/stable-v1.1.0-green.svg)](https://shields.io/) [![Generic badge](https://img.shields.io/badge/BUILD-OK-<COLOR>.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/TESTS-OK-<COLOR>.svg)](https://shields.io/)

> "You will not be able to stay home, brother
You will not be able to plug in, turn on and cop out
You will not be able to lose yourself on skag and skip out for beer during commercials, because
The revolution will not be televised ..." Gil Scot Heron

## What is it :

- A Golang JSON Api with multiple usefull routes around exchanges information gathering and statistics computing.

- A Dart with Flutter [mobile client](./sentinel/README.md) (Android/Ios) who turns the heavy api talks into a easy understandable set off UI elements.

## What it isn't :

- A malware (you got the whole sources)
- Not yet a *production ready* product*

*🐦 Follow updates on [`twitter`]() to get the latest about `sentinel` journey !

## Whats its goals ?
- Sentinel is a personnal **fund manager**, 
- He is helping you to build **your strategy**,
- ...  right from your fingertips in the [mobile application](./sentinel/README.md) or
- ...  **any client** you like with the [go api](./main.go) 

When enabled with **your favorite exchanges**\* api keys :

- He is **monitoring** your markets positions, 
- ... helping you to take the **best actions** in no time
- ... for your **assets to grow** successfully.

\*As of 03/09/2019 only **Binance** api is supported

## Why is it a thing :

- Because existing FOSS **assets investments toolsets** are cumbersome,

- Because a locally setupable product allow you to keep a **peacefull control over your datas**

- Because sentinel is available **ready to run** on every kind of platform,

- ... from your personnal smartphone to a more complex architecture on cloud services.


## Who is behind this :

Original author :

=> `Dave Lopeur [dave.lopeur@protonmail.com]` from the `LopeurFamilly©`

Current maintener :

=> `Dave ` 'codeLessDoMore' `Lopeur [dave.lopeur@protonmail.com]` also from the `LopeurFamilly©`

Complaints :

=> `Dave Null [dontsend@devnull.com` from `DevNull©` ;)

____

# A. Install

## 1) General

### `Sentinel` Mobile App bundle :

| Os | Download | Documentation |
| -- |    --    |      --       |
| Android | F-Droid / PlayStore / [.apk]() | WIP |
| Ios | App Store / [Ask for a quick build]() | WIP |

### `Sentinel` Desktop bundle :

🔎 If in doubt, choose these packages as they are fully ready to run with every tools from sentinel straight from the box.

| Os | Download | Documentation |
| -- | --| --- |
| Windows| [.exe](https://doc.sentinel.org/download/for/windows) |https://doc.sentinel.org/install/for/windows|
|Mac| [.dmg](https://doc.sentinel.org/download/for/macos) / [homebrew](https://doc.sentinel.org/install/with/macos) |https://doc.sentinel.org/install/for/macos|
|BSD| [.tar.gz](https://doc.sentinel.org/download/for/unix) / [package managers](https://doc.sentinel.org/download/for/unix) |https://doc.sentinel.org/install/for/bsd|
| Unix| [.tar.gz](https://doc.sentinel.org/download/for/unix)  / [package managers](https://doc.sentinel.org/download/for/unix) | RTFM  |https://doc.sentinel.org/install/for/unix|


#### `server` Api cli :

Unix : https://doc.sentinel.org/serve/for/unix

BSD : https://doc.sentinel.org/serve/for/bsd

Windows : https://doc.sentinel.org/serve/for/windows

### `sentinel` WebApp gui :

Unix : https://doc.sentinel.org/view/for/unix

BSD : https://doc.sentinel.org/view/for/bsd

Windows : https://doc.sentinel.org/view/for/windows




## 2) Developement

Straight from your favorite command prompt, this pseudo script might give you an idea of what to do before running everything locally :

```bash
# Install the whole project deps :
sudo apt-get install \
    # Basic tools (build essential is gcc toolchain)
    curl git build-essential \
    # Langages suports packages who might need 
    # adding an extra repo before
    golang  flutter \
    # For database with no hassle
    docker
# Start API in dev mode
make
# Start Mobile Client in dev
cd sentinel && flutter package get && \
 flutter run
```


# B. Push contribution


Code :

See repository issues !

Lint :
```bash
# For `server`
go lint  # (or whatever)
# for flutter :
flutter lint # (idem )
```

Test :
```bash
# <component> = "server" or "mobile"  
make test <component> &&  \
    cat results.json | jq .
```

Commit : 

```bash
# Be sure to have a correct git config setup (user.name, user.email)
# mostly for easier mail communication (run git config --list if unsure)
git commit -m "<type>(<scope>): <subject>"
```

Pull :

```bash
# Be sure to have a correct git config setup (user.name, user.email)
# mostly for easier mail communication (run git config --list if unsure)
git push origin myawesomefork
```

⚠ After sending a pull request, you must send a brief email 
to ~~`checkme@pr.sentinel.org`~~ `current maintener email` with **content** :

```txt
----
PR(<pullrequest id>): <github username or email> <commitSHA>
----
TYPE(<fix,feat or refacto>): <brief description, you could paste commit msg>
----
TEST(<commitSHA>): <insert TRUE or FALSE if you want the test result emailed back to your address>
----
<Free text field, feel free to write comments here, they will be directly slack'ed to the maintener :) (if you want the whole process to be fast, this might a very good thing to do btw !)>
END
```
___

### Made with ❤️, ⌛ and ☕ in Bordeaux FR ; Written in **Go** & **Dart**





