module sentinel

go 1.14

require (
	github.com/BillotP/coinbase v0.0.0-20200501212306-f8afb32b79c4
	github.com/adshao/go-binance v0.0.0-20200414012312-338a1df204bf
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/tidwall/pretty v1.0.1 // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.3.2
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
